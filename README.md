# Ajax Forms

Ajax Forms handles async request to the server. Ajax Forms will only handle the request. It will not perform any other action (like preventing default form actions).

### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="ajax-forms.js"></script>
```

Then hook some actions to the script events:

```javascript
// Initializes ajax forms
var form = new CRD.AjaxForm('#form');

// Hooks some events
form.on('crd.ajaxforms.success', function(data, status, xhr) {
	console.log('Form sent successfully!');
});

// Prevent default form action and triggers ajax forms
jQuery('#submit').on('click', function(event) {
	event.preventDefault();
	form.send();
});
```
*A packed and merged source is available to use.*

### Arguments

All Images Loaded accepts two arguments:

- ```element``` - default to ```document.body``` - container of the images and background images that the script must hook to.
- ```options``` - default to ```{}``` - options to overwrite default values.

### Dependencies

- JQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details