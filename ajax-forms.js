/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD');

CRD.AjaxForm = (function() {
	"use strict";
	
	// Variables
	var storage = {
			main : 'crd.ajaxform'
		}, // Storage
		constants = {
			Events : {
				SUCCESS : 'crd.ajaxform.success',
				ERROR   : 'crd.ajaxform.error'
			}
		}, // Constants
		extend = jQuery.extend; // jQuery.extend shortcut
	
	/**
	 * AjaxForm
	 * @class
	 * @param {Object} element - Form element.
	 * @param {Object} options - Options to override default options.
	 */
	
	function AjaxForm(element, options) {
		
		// Element reference
		element = jQuery(element);
		
		// Variables
		var self = element.data(storage.main); // Data storage reference
		
		// If instance hasn't been created yet
		if(typeof self === 'undefined') {
			
			// Set options and events
			this.setOptions(options);
			
			/**
			 * Element to use as layout element
			 * @property {Object} element - Element
			 */
			
			this.element = element;
			
			// Stores the object reference in the element storage
			this.element.data(storage.main, this);
			
			// Sets data reference
			self = this;
			
		}
		
		// Returns object reference
		return self;
		
	}
	
	// Extends the base prototype
	AjaxForm.prototype = extend({
		
		/**
		 * Default options
		 * @property {Object} defaults           - Default options
		 * @property {string} defaults.method    - Default request method
		 * @property {Object} defaults.dataType  - Expected response data type
		 * @property {Object} defaults.extraData - Additional data to send with the request
		 */
		
		defaults : {
			method    : 'post',
			dataType  : 'json',
			extraData : {}
		},
		
		/**
		 * Send request
		 * @method send
		 * @fires CRD.AjaxForm.Events.SUCCESS
		 * @fires CRD.AjaxForm.Events.ERROR
		 * @memberof AjaxForms
		 */
		
		send : function() {
			
			// Variables
			var extra = Object.keys(this.options.extraData).length > 0 ?
					'&' + jQuery.param(extend({}, this.options.extraData)) :
					'',
				request;
			
			// Send ajax request
			request = new jQuery.ajax({
				url      : this.element.attr('action'),
				method   : this.element.attr('method') || this.options.method,
				dataType : this.options.dataType,
				cache    : false,
				data     : this.element.serialize() + extra,
				error    : function(xhr, status, error) {
					
					/**
					 * Triggers events
					 * @event CRD.AjaxForm.Events.ERROR
					 */
					
					this.dispatch(constants.Events.ERROR, [
						xhr,
						status,
						error
					]);
					
				}.bind(this),
				success  : function(data, status, xhr) {
					
					/**
					 * Triggers events
					 * @event CRD.AjaxForm.Events.SUCCESS
					 */
					
					this.dispatch(constants.Events.SUCCESS, [
						data,
						status,
						xhr
					]);
					
				}.bind(this)
			});
			
			// Return the request
			return request;
			
		}
		
	}, CRD.ClassUtils);
	
	// Set object constants
	CRD.Utils.setConstants(AjaxForm, constants);
	
	// Return the class
	return AjaxForm;
	
})();